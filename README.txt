
         ,---._                                      ,-.----.
       .-- -.' \                                     \    /  \                                  ___
       |    |   :                                    |   :    \            ,--,               ,--.'|_
       :    ;   |                                    |   |  .\ :         ,--.'|        ,---,  |  | :,'           __  ,-.
       :        |              .---.                 .   :  |: |         |  |,     ,-+-. /  | :  : ' :         ,' ,'/ /|
       |    :   : ,--.--.    /.  ./|  ,--.--.        |   |   \ :,--.--.  `--'_    ,--.'|'   .;__,'  /    ,---. '  | |' |
       :         /       \ .-' . ' | /       \       |   : .   /       \ ,' ,'|  |   |  ,"' |  |   |    /     \|  |   ,'
       |    ;   .--.  .-. /___/ \: |.--.  .-. |      ;   | |`-.--.  .-. |'  | |  |   | /  | :__,'| :   /    /  '  :  /
   ___ l         \__\/: . .   \  ' . \__\/: . .      |   | ;   \__\/: . .|  | :  |   | |  | | '  : |__.    ' / |  | '
 /    /\    J   :," .--.; |\   \   ' ," .--.; |      :   ' |   ," .--.; |'  : |__|   | |  |/  |  | '.''   ;   /;  : |
/  ../  `..-    /  /  ,.  | \   \   /  /  ,.  |      :   : :  /  /  ,.  ||  | '.'|   | |--'   ;  :    '   |  / |  , ;
\    \         ;  :   .'   \ \   \ ;  :   .'   \     |   | : ;  :   .'   ;  :    |   |/       |  ,   /|   :    |---'
 \    \      ,'|  ,     .-./  '---"|  ,     .-./     `---'.| |  ,     .-.|  ,   /'---'         ---`-'  \   \  /
  "---....--'   `--`---'            `--`---'           `---`  `--`---'    ---`-'                        `----'




+--------------+
|   License    |
+--------------+

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `agpl-3.0.txt` in the root directory of this project and can also be found online here:
https://opensource.org/licenses/AGPL-3.0




+--------------+
| System Specs |
+--------------+

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the toolsets shouldn't break the build.

+--------------------------+--------------------------+
|         Software         |          Version         |
+--------------------------+--------------------------+
| Java Runtime Environment |  build 1.8.0_212-b10 x64 |
|   Java Development Kit   |    build 1.8.0_212 x64   |
|          Gradle          |           5.4.1          |
|      Windows 10 Pro      |         1903 x64         |
+--------------------------+--------------------------+




+---------+
|  Usage  |
+---------+

- [X] Executing `gradle build` will build *Painter*. You can find the generated fat jar file in `/build/libs`; please
ensure that the `README.txt` and `LICENSE` files are in the same directory as the fat jar file when you run it.
- [x] Executing `gradle run` will run *Painter*. Note that is not necessary to run the `build` command above; you can
simply execute the `run` command to build and start the application.
- [x] Included in the root directory of this project is the archive `Painter-1.1.zip` which has a self-contained fat jar
 file. To run the application simply extract the contents of the archive, open a command prompt and navigate to where
 you extracted the files. Running the command `java -jar Painter-1.1.jar` will launch the application.




+----------+
| Features |
+----------+

##### File Operations:

The data written to a file from the canvas is written as a raw Java `Object` format and is thus not human readable. To
write the file to disk *Painter* is using `Object Serializable` interface, so every time the application is recompiled
any files written with an "older" version of the application becomes unreadable by the new build. Thus any file that is
Loaded must be Saved with the same build.

- [x] New: This will clear the scene and create a blank canvas.
- [x] Open: This will open a saved `.painter` file created with the same build.
- [x] Save: This will save a `.painter` file using raw `Serialized` scene data.



##### Tool Palette:

## Select Tool:
- [x] Select shape: Clicking on a shape will bring it to the top of all of the other shapes on the canvas, and this is
      by design. It isn't efficient as the underlying data structure containing the scene data is an `ArrayList` and
      performing this action requires shifting the array elements down to fill in the gap created. I felt it was
      essential to allow shapes to be reordered on the screen.
- [x] Deselect shape: Click anywhere on the canvas, or another shape, and it will deselect the shape and return it to
      the solid black border.
- [x] ESC: Hitting the `ESC` key will deselect the currently selected shape.
- [x] Canvas Indication: Selecting a shape will bring it to the top of all other shapes but also switch its border to a
      dashed line which will change thickness based on the actual border thickness applied to the shape.
- [x] Tool Palette Indication: The colour and thickness of the shape selected will update the respective swatches in
      the tool palette.
- [x] Change Border: Selecting a shape and adjusting the thickness will change the thickness to the one selected. The
      on screen shape will have a dashed border to indicate selection with a border thickness corresponding to the
      thickness selected.
- [x] Change Colour: Selecting a shape and clicking on a colour swatch will update the colour of the shape.
- [x] Moving: Selecting a shape and dragging will move it. A shape can only be moved if it has been clicked on and
      selected first, after which you can click and drag anywhere on the screen and it will move in the corresponding
      direction and distance.
- [x] Live View: The movements, colour, and thickness will be updated on the canvas in realtime.


## Eraser Tool:
- [x] Select: With the tool active click on a shape to remove it from the scene.


## Line Tool:
- [x] Draw: Click on a start point and drag in any direction to create a line in that direction, release the mouse to
      complete the line in the colour and thickness selected.
- [x] Live View: The line will be updated on the canvas in realtime as it's being drawn.


## Ellipse Tool:
- [x] Draw: Click on a start point and drag in any direction to create an ellipse in that direction, release the mouse
      to complete the ellipse in the colour and thickness selected.
- [x] Live View: The ellipse will be updated on the canvas in realtime as it's being drawn.


## Rectangle Tool:
- [x] Draw: Click on a start point and drag in any direction to create a rectangle in that direction, release the mouse
      to complete the rectangle in the colour and thickness selected.
- [x] Live View: The rectangle will be updated on the canvas in realtime as it's being drawn.


## Fill Tool:
- [x] Fill: Click on a shape to fill the shape under the mouse pointer with the selected colour.
- [x] Live View: The colour fill should be updated on the canvas in realtime as it's being executed.
- [x] Line: A line should not be a fill-able object. The only way to change a lines colour remains via the
      `Select Tool`.


##### Colour Palette:
- [x] Predefined: Selecting a colour from the predefined colour swatch will set the line or shapes colour when it’s
      drawn, selected or has a fill applied.
- [x] Custom: Clicking on the swatch will open a colour selector dialog allowing the user to select a custom colour.
      On successful selection the swatch and the active tool colour will be set to the colour.



##### Thickness Palette:
- [x] Thickness: Allows the user to switch between four predefined border line thicknesses of 2px, 5px, 10px and 15px.



##### Extra Feature(s):
- [x] Copy: Copies the active canvas as an ARGB colour definition image to the system level clipboard so it can be
      pasted into another application. Access `Copy` fom the `Edit` menu in the main toolbar.




+-------------------------------+
| Copyright and Legal Notice(s) |
+-------------------------------+

© Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

Icons made by Freepik from www.flaticon.com is licensed by CC 3.0 BY.
