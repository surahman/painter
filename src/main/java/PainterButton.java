            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    PainterButton.java              #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #          Abstract JToggleButton Class.            #
            #__________________________________________________*/



//- Import(s): Library:
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/


abstract class PainterButton extends JToggleButton {

    /* PainterButton:
     *  Default constructor.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    PainterButton()
    {
    }



    /* SetupGroups:
     *  Set up the various button group references for internal use.
     * -Parameters:  ButtonGroup tool, ButtonGroup colour, ButtonGroup thickness
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public static void SetupGroups(ButtonGroup tool, ButtonGroup colour, ButtonGroup thickness)
    {
        m_ToolGroup = tool;
        m_ColourGroup = colour;
        m_ThicknessGroup = thickness;
    }



    /* LoadIcon:
     *  Load an <Icon> from the <resources> subdirectory.
     * -Parameters:  final String k_BaseDir, final String k_IconName
     * -Returns:     Icon
     * -Throws:      n/a
     */
    public static Icon LoadIcon (final String k_BaseDir, final String k_IconName)
    {
        //-- Variable Declaration(s):
        InputStream istream = null;             // Input stream for icon image.
        Icon icon = null;                       // Loaded <icon> image for button.

        try
        {
            // Setup <icon>.
            istream = PainterButton.class.getClass().getResourceAsStream(k_BaseDir + k_IconName);
            if (istream == null) { throw new IOException(); }

            icon = new ImageIcon(ImageIO.read(istream));
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Failed to load " + k_IconName + " : " + e.toString());
        }

        return icon;
    }



    /* LoadImageIcon:
     *  Load an <ImageIcon> from the <resources> subdirectory.
     * -Parameters:  final String baseDir, final String iconName
     * -Returns:     ImageIcon
     * -Throws:      n/a
     */
    public static ImageIcon LoadImageIcon (final String baseDir, final String iconName)
    {
        //-- Variable Declaration(s):
        InputStream istream = null;             // Input stream for icon image.
        ImageIcon icon = null;                  // Loaded <icon> image for button.

        try
        {
            // Setup <icon>.
            istream = PainterButton.class.getClass().getResourceAsStream(baseDir + iconName);
            if (istream == null) { throw new IOException(); }

            icon = new ImageIcon(ImageIO.read(istream));
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Failed to load " + iconName + " : " + e.toString());
        }

        return icon;
    }



    /* LoadAppIcon:
     *  Load all the application <BufferedImage>s from the <resources> subdirectory.
     * -Parameters:  final String baseDir
     * -Returns:     List<BufferedImage>
     * -Throws:      n/a
     */
    public static List<BufferedImage> LoadAppIcon (final String baseDir)
    {
        //-- Variable Declaration(s):
        InputStream istream = null;             // Input stream for icon image.
        List<BufferedImage> appIcons = null;    // <ArrayList> containing application icons.

        try
        {
            // Read all file names from resource directory.
            istream = PainterButton.class.getClass().getResourceAsStream(baseDir);
            if (istream == null) { throw new IOException(); }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(istream));

            // Load icons into <ArrayList>.
            String filename;
            appIcons = new ArrayList<>();
            while ( ( filename = bufferedReader.readLine() ) != null)
            {
                InputStream imageStream = PainterButton.class.getClass().getResourceAsStream(baseDir + filename);
                if (imageStream == null) { throw new IOException(); }

                appIcons.add(ImageIO.read(imageStream));
            }
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Failed to load application icons in " + baseDir + " : " + e.toString());
        }

        return appIcons;
    }


    //-- Data Member(s):
    protected static ButtonGroup m_ToolGroup = null;        // Reference to the Tool Group.
    protected static ButtonGroup m_ColourGroup = null;      // Reference to the Colour Group.
    protected static ButtonGroup m_ThicknessGroup = null;   // Reference to the Thickness Group.



    //-- Enum(S) & Namespace(s):
    public enum Tool                            // Tool palette space.
    {
        Select,                                 // Select tool to manipulate objects.
        Eraser,                                 // Eraser tool.
        Line,                                   // Draw line tool.
        Ellipse,                                // Draw ellipse tool.
        Rectangle,                              // Draw rectangle tool.
        Fill,                                   // Fill tool to colour center of closed shape.
    }


    public enum Colours                         // Colour palette space.
    {
        Black,                                  // Cell #1
        Pink,                                   // Cell #2
        Red,                                    // Cell #3
        Green,                                  // Cell #4
        Blue,                                   // Cell #5
        Yellow,                                 // Cell #6
        Cyan,                                   // Cell #7
        Magenta,                                // Cell #8
        Custom,                                 // Cell #9
    }


    public enum Thickness                       // Border thickness space
    {
        Thin        (2),                        // 1 px.
        Normal      (5),                        // 5 px.
        Thick       (10),                       // 10 px.
        Thicker     (15),                       // 15 px.
        ;                                       // List terminator.

        //-- Constructor
        private Thickness (int thickness)
        {
            k_Thickness = thickness;
        }

        //-- Method(s):
        public int getSize()
        {
            return k_Thickness;
        }

        //-- Data Member(s:
        private final int k_Thickness;
    }
}
