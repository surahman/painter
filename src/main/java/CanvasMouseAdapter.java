            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    CanvasMouseAdapter.java         #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #        Java Painter Mouse Adapter Class.          #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class CanvasMouseAdapter extends MouseAdapter {

    /* CanvasMouseMotionListener:
     *  Constructor for mouse movement capture.
     * -Parameters:  Canvas canvas
     * -Returns:     n/a
     * -Throws:      n/a
     */
    CanvasMouseAdapter(Canvas canvas)
    {
        k_CanvasRef = canvas;
    }



    /* mouseClicked:
     *  Click and drag mouse events.
     * -Parameters:  MouseEvent event
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    public void mouseClicked(MouseEvent event)
    {
        k_CanvasRef.clickedHandler(event.getPoint());
    }


    /* mouseDragged:
     *  Click and drag mouse events.
     * -Parameters:  MouseEvent event
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    public void mouseDragged(MouseEvent event)
    {
        k_CanvasRef.draggedHandler(event.getPoint());
    }



    /* mousePressed:
     *  Mouse button depressed events.
     * -Parameters:  MouseEvent event
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    public void mousePressed(MouseEvent event)
    {
        k_CanvasRef.setClickedPoint(event.getPoint());
    }



    /* mouseReleased:
     *  Mouse button released events.
     * -Parameters:  MouseEvent event
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    public void mouseReleased(MouseEvent event)
    {
        k_CanvasRef.completeShape();
        k_CanvasRef.setClickedPoint(null);
    }



    //-- Data Member(s):
    private final Canvas k_CanvasRef;           // Reference to <Canvas> object.
}
