            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ThicknessButton.java            #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #          Thickness JToggleButton Class.           #
            #__________________________________________________*/



//- Import(s): Library:
import javax.swing.*;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class ThicknessButton extends PainterButton {



    /* ToolButton:
     *  Tool panel button constructor.
     * -Parameters:  Canvas canvas, Color backgroundColour, Icon icon, Thickness thickness
     * -Returns:     n/a
     * -Throws:      n/a
     */
    ThicknessButton(Canvas canvas, Color backgroundColour, Icon icon, Thickness thickness)
    {
        //-- Variable Declaration(s):
        int height = 0;                         // Height of <icon> to setup <button> height.
        int width = 0;                          // Width of <icon> to setup <button> width.


        // Setup button look and feel.
        this.setPreferredSize(new Dimension(100, 32));
        this.setBorderPainted(false);
        this.setIcon(icon);
        this.setBackground(backgroundColour);

        // Set buttons depressed state colour.
        this.setUI(
            new MetalToggleButtonUI()
            {
                @Override
                protected Color getSelectColor()
                {
                    return Color.ORANGE;
                }
            }
        );

        // Add to the button group.
        m_ThicknessGroup.add(this);

        // On-click update colour.
        this.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    // Update the <Canvas> colour.
                    canvas.setThickness(thickness);
                }
            }
        );
    }
}
