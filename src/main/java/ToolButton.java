            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ToolButton.java                 #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #             Tool JToggleButton Class.             #
            #__________________________________________________*/



//- Import(s): Library:
import javax.swing.*;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class ToolButton extends PainterButton {



    /* ToolButton:
     *  Tool panel button constructor.
     * -Parameters:  Canvas canvas, String toolIconDir, String iconName, Tool toolName
     * -Returns:     n/a
     * -Throws:      n/a
     */
    ToolButton(Canvas canvas, String toolIconDir, String iconName, Tool toolName)
    {
        //-- Variable Declaration(s):
        Icon icon = null;                       // Loaded <icon> image for button.
        int height = 0;                         // Height of <icon> to setup <button> height.
        int width = 0;                          // Width of <icon> to setup <button> width.


        // Setup <icon>.
        icon = LoadIcon(toolIconDir, iconName);
        height = icon.getIconHeight();
        width = icon.getIconWidth();

        // Setup <button> parameters.
        this.setPreferredSize(new Dimension(width, height));
        this.setIcon(icon);

        // Set buttons depressed state colour.
        this.setUI(
            new MetalToggleButtonUI()
            {
                @Override
                protected Color getSelectColor()
                {
                    return Color.ORANGE;
                }
            }
        );

        // Add to the button group.
        m_ToolGroup.add(this);


        // On-click update colour.
        this.addActionListener(
            new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    // Update the <Canvas> colour.
                    canvas.setTool(toolName);
                }
            }
        );
    }
}
