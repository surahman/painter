            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ColourSelectorButton.java       #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #       Colour Selector JToggleButton Class.        #
            #__________________________________________________*/



//- Import(s): Library:
import javax.swing.*;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/


public class ColourSelectorButton extends PainterButton {



    /* ColourSelectorButton:
     *  Custom colour panel button constructor.
     * -Parameters:  Canvas canvas, PainterButton.Colours colourID, Color backgroundColour, Icon selected, Icon deselected
     * -Returns:     n/a
     * -Throws:      n/a
     */
    ColourSelectorButton(Canvas canvas,
                         Color backgroundColour,
                         PainterButton.Colours colourID,
                         Icon selected,
                         Icon deselected)
    {
        this.setIcon(deselected);
        this.setSelectedIcon(selected);
        this.setBackground(backgroundColour);

        // Set buttons depressed state colour to transparent.
        this.setUI(
            new MetalToggleButtonUI()
            {
                @Override
                protected Color getSelectColor()
                {
                    return new Color(0, 0, 0, 0);
                }
            }
        );

        // Setup the colour group.
        m_ColourGroup.add(this);


        // On-click open colour selector.
        this.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    Color chosenColour = getBackground();
                    chosenColour = JColorChooser.showDialog(null, "Pick custom colour", chosenColour);

                    // Check to see if colour was chosen.
                    if (chosenColour == null)
                    {
                        chosenColour = canvas.getFillColour();
                    }

                    // Update the buttons colour.
                    setBackground(chosenColour);

                    // Update the <Canvas> colour.
                    canvas.setColour(chosenColour, colourID);
                }
            }
        );
    }

}
