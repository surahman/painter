            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    PainterMain.java                #
            # Dependencies :    LIB:                            #
            #                   USR: Painter.java               #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #             Java Painter Entry Point.             #
            #__________________________________________________*/



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class PainterMain {

    public static void main (String[] args) {


        try
        {

            // Validate argument count.
            if (args.length != 0 && args.length != 2 )
            {
                throw new IllegalArgumentException("Usage: Illegal argument count, please see documentation.");
            }


            //-- Variable Declaration(s):
            final int maxWidth = 1600;          // Maximum window width.
            final int maxHeight = 1200;         // Maximum window height.
            int width = maxWidth;               // Window width.
            int height = maxHeight;             // Window height.


            // Validate and setup custom window dimensions.
            if (args.length == 2)
            {

                // Convert parameters to integers and validate.
                width = Integer.parseInt(args[0]);
                height = Integer.parseInt(args[1]);

                if (width <= 0 || width > maxWidth || height <= 0 || height > maxHeight)
                {
                    throw new IllegalArgumentException("Invalid window dimension parameters passed.");
                }
            }


            // Launch paint application.
            Painter Painter = new Painter(width, height);

        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
