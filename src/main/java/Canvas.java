            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Canvas.java                     #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Java Painter Canvas Class.             #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.*;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Canvas extends JPanel {

    /* Canvas:
     *  Actual drawing model.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Canvas(Dimension canvasDims)
    {
        // Set default tool states.
        m_ActiveTool = PainterButton.Tool.Line;
        m_ActiveThickness = PainterButton.Thickness.Normal;
        m_ActiveColourType = PainterButton.Colours.Black;
        m_ActiveFillColour = Color.BLACK;
        m_ActiveStrokeColour = Color.BLACK;

        // Initialize scene array.
        m_SceneData = new ArrayList<>(25);

        // Setup mouse listeners.
        CanvasMouseAdapter mouseListener = new CanvasMouseAdapter(this);
        this.addMouseListener(mouseListener);
        this.addMouseMotionListener(mouseListener);

        // Add accelerator event handler to deselect on <ESC> key.
        // Help: <stackoverflow> #2: https://stackoverflow.com/questions/286727/unresponsive-keylistener-for-jframe
        KeyboardFocusManager KBFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        KBFocusManager.addKeyEventDispatcher(
            new KeyEventDispatcher()
            {
                @Override
                public boolean dispatchKeyEvent(KeyEvent event) {

                    // Deselect shapes and repaint.
                    if (event.getKeyCode() == KeyEvent.VK_ESCAPE)
                    {
                        m_ActiveShapeID = -1;
                        repaint();
                    }

                    return false;
                }
            }
        );


        // Add rectangle.
        add(PainterButton.Tool.Rectangle, PainterButton.Colours.Red,Color.RED, PainterButton.Thickness.Thicker, 50, 200, 400, 400);

        // Add Circle.
        add(PainterButton.Tool.Ellipse, PainterButton.Colours.Green, Color.GREEN, PainterButton.Thickness.Normal, 425, 200, 400, 400);

        // Draw triangle.
        add(PainterButton.Tool.Line, PainterButton.Colours.Blue, Color.BLUE, PainterButton.Thickness.Normal, 725, 600, 1125, 600);
        add(PainterButton.Tool.Line, PainterButton.Colours.Blue, Color.BLUE, PainterButton.Thickness.Normal, 725, 600, 925, 200);
        add(PainterButton.Tool.Line, PainterButton.Colours.Blue, Color.BLUE, PainterButton.Thickness.Normal, 925, 200, 1125, 600);


        // Put empty drawing shape in scene.
        m_SceneData.add(null);
    }



    /* paintComponent:
     *  Sends request to draw to the Java 2D render engine.
     * -Parameters:  Graphics graphics
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    protected void paintComponent(Graphics graphics)
    {
        super.paintComponent(graphics);

        // Setup <Scene> containing all 2D graphics to send to render engine.
        Graphics2D Scene2D = (Graphics2D) graphics;
        Scene2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Iterate over scene data and redraw.
        for (int idx = 0; idx < m_SceneData.size(); ++idx)
        {

            // Avoid nullptr exceptions.
            if (m_SceneData.get(idx) == null)
            {
                continue;
            }


            // Fill <shape>.
            if (m_SceneData.get(idx).m_Type != PainterButton.Tool.Line)
            {
                Scene2D.setPaint(m_SceneData.get(idx).m_FillColour);
                Scene2D.fill(m_SceneData.get(idx).m_Shape);
            }


            // Set stroke colour.
            if (m_SceneData.get(idx).m_Type == PainterButton.Tool.Line)
            {
                Scene2D.setPaint(m_SceneData.get(idx).m_FillColour);
            }
            else {
                Scene2D.setPaint(m_ActiveStrokeColour);
            }


            // Set appropriate stroke.
            if (m_ActiveTool == PainterButton.Tool.Select && idx == m_ActiveShapeID)
            {
                float pattern[] = {m_SceneData.get(idx).m_Thickness.getSize(),
                                   m_SceneData.get(idx).m_Thickness.getSize() * 2};

                BasicStroke dashed = new BasicStroke(pattern[0],
                                                     BasicStroke.CAP_BUTT,
                                                     BasicStroke.JOIN_ROUND,
                                                    5.0f,
                                                     pattern,
                                                    0.0f);
                Scene2D.setStroke(dashed);
            }
            else
            {
                Scene2D.setStroke(new BasicStroke(m_SceneData.get(idx).m_Thickness.getSize()));
            }


            // Draw the scene.
            Scene2D.draw(m_SceneData.get(idx).m_Shape);
        }
    }



    /* setTool:
     *  Set the currently active tool to the selected one.
     * -Parameters:  PainterButton.Tool tool
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setTool(PainterButton.Tool tool)
    {
        deselectShape();
        m_ActiveTool = tool;
    }



    /* setColour:
     *  Set the currently active fill colour to the selected one.
     * -Parameters:  Color colour
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setColour(Color colour, PainterButton.Colours colourID)
    {
        m_ActiveFillColour = colour;
        m_ActiveColourType = colourID;

        // Update the selected shapes colour.
        if (m_ActiveTool == PainterButton.Tool.Select && m_ActiveShapeID != -1)
        {
            m_SceneData.get(m_ActiveShapeID).m_FillColour = m_ActiveFillColour;
            m_SceneData.get(m_ActiveShapeID).m_ColourType = m_ActiveColourType;
            repaint();
        }
    }



    /* setThickness:
     *  Set the currently active tool thickness to the selected one.
     * -Parameters:  BorderThickness thickness
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setThickness(PainterButton.Thickness thickness)
    {
        m_ActiveThickness = thickness;

        // Update the selected shapes border thickness.
        if (m_ActiveTool == PainterButton.Tool.Select && m_ActiveShapeID != -1)
        {
            m_SceneData.get(m_ActiveShapeID).m_Thickness = m_ActiveThickness;
            repaint();
        }
    }



    /* getFillColour:
     *  Get the currently active fill colour.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public Color getFillColour()
    {
        return m_ActiveFillColour;
    }



    /* add:
     *  Add a new shape to the currently active scene. This routine was used for internal testing.
     * -Parameters:  PainterButton.Tool type, Color colour, PainterButton.Thickness thickness, int x, int y, int height, int width
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void add(PainterButton.Tool type,
                     PainterButton.Colours colourType,
                     Color colour,
                     PainterButton.Thickness thickness,
                     double x,
                     double y,
                     double width,
                     double height)
    {
        m_SceneData.add(new JSShape(type, colourType, colour, thickness, x, y, height, width));
        repaint();
    }



    /* add:
     *  Add a new shape to the currently active scene using current settings.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void add()
    {
        // Create shape without <Shape> object.
        JSShape newShape = new JSShape();
        newShape.m_Type = m_ActiveTool;
        newShape.m_ColourType = m_ActiveColourType;
        newShape.m_FillColour = m_ActiveFillColour;
        newShape.m_Thickness = m_ActiveThickness;
    }



    /* clear:
     *  Clear/wipe the canvas clean and redraw it.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    void clear()
    {
        m_SceneData.clear();
        m_SceneData.add(null);
        this.repaint();
    }



    /* registerToolPalette:
     *  Create reference to <ToolPalette> object.
     * -Parameters:  ToolPalette toolPalette
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void registerToolPalette(ToolPalette toolPalette)
    {
        m_ToolPalette = toolPalette;
    }



    /* selectShape:
     *  Given coordinates select the first shape that contains it.
     * -Parameters:  Point location
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void selectShape(Point location)
    {
        // Reset selected shape ID for deselect operation.
        m_ActiveShapeID = -1;

        // Setup mouse hotspot.
        int hotspotDim = 3;
        int hotspotEdge = hotspotDim * 2;
        double hotspotX = location.getX() - hotspotDim;
        double hotspotY = location.getY() - hotspotDim;

        // Locate shape in list, last item is <null> shape that is going to be drawn.
        for (int idx = m_SceneData.size() - 2; idx >= 0; --idx)
        {
            // Check if point is contained and set it as selected.
            if (m_SceneData.get(idx).m_Shape.intersects(hotspotX, hotspotY, hotspotEdge, hotspotEdge))
            {
                // Update local state.
                m_ActiveShapeID  = idx;

                break;
            }
        }
    }



    /* clickedHandler:
     *  Handle mouse clicks and dispatch appropriately.
     * -Parameters:  Point location
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void clickedHandler(Point location)
    {
        // Select object under clicked cursor.
        selectShape(location);

        // Dispatch to correct tool.
        if (m_ActiveShapeID != -1)
        {
            switch (m_ActiveTool)
            {
                case Select:
                    selectHandler();
                    break;
                case Eraser:
                    eraseHandler();
                    break;
                case Fill:
                    fillHandler();
                    break;
                default:
                    break;
            }
        }


        repaint();
    }



    /* clickedHandler:
     *  Handle mouse drags and dispatch appropriately.
     * -Parameters:  Point location
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void draggedHandler(Point location)
    {
        // Dispatch to correct tool.
        switch (m_ActiveTool)
        {
            case Select:
                moveHandler(location);
                break;
            case Line:
            case Ellipse:
            case Rectangle:
                drawHandler(location);
                break;
            default:
                break;
        }

        repaint();
    }



    /* deselectShape:
     *  Handle deselect operations and redraw.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void deselectShape()
    {
        if (m_ActiveShapeID != -1)
        {
            m_ActiveShapeID = -1;
            repaint();
        }
    }



    /* selectShape:
     *  Handle select operations (colour and thickness) on shape.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void selectHandler()
    {
        // Safety Check(s):
        if (m_ActiveShapeID == -1)
        {
            return;
        }

        // Update active shapes attributes.
        m_ActiveFillColour = m_SceneData.get(m_ActiveShapeID).m_FillColour;
        m_ActiveThickness = m_SceneData.get(m_ActiveShapeID).m_Thickness;
        m_ActiveColourType = m_SceneData.get(m_ActiveShapeID).m_ColourType;

        // Notify the <ToolPalette> of the updated shape attributes.
        m_ToolPalette.notify(m_ActiveThickness, m_ActiveColourType, m_ActiveFillColour);


        // Move selected shape to the top. This is very inefficient because the entire array is relocated piece-wise.
        JSShape shape = m_SceneData.remove(m_ActiveShapeID);            // Remove shape.
        m_SceneData.add(m_SceneData.size() - 1, shape);           // Insert in front of null shape (next new shape).
        m_ActiveShapeID = m_SceneData.size() - 2;                       // Set active shapes id to correct index.

    }



    /* eraseHandler:
     *  Remove the shape marked currently active.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void eraseHandler()
    {
        m_SceneData.remove(m_ActiveShapeID);
    }



    /* fillHandler:
     *  Fill the shape marked currently active.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void fillHandler()
    {
        // Lines should not be fill-able.
        if (m_SceneData.get(m_ActiveShapeID).m_Type == PainterButton.Tool.Line)
        {
            return;
        }

        m_SceneData.get(m_ActiveShapeID).m_FillColour = m_ActiveFillColour;
        m_SceneData.get(m_ActiveShapeID).m_ColourType = m_ActiveColourType;
    }


    /* moveHandler:
     *  Move selected shape using pointer.
     * -Parameters:  Point location
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void moveHandler(Point location)
    {

        // Terminate if no shape selected.
        if (m_ActiveShapeID == -1)
        {
            return;
        }


        // Call and update to the <Tool Palette>.
        selectHandler();

        // Calculate offset to move point.
        double xDelta = m_BaseLocation.getX() - location.getX();
        double yDelta = m_BaseLocation.getY() - location.getY();

        // All drawn shapes are bounded inside of a box, extract dimensions.
        JSShape reference = m_SceneData.get(m_ActiveShapeID);
        Rectangle2D boundingBox = reference.m_Shape.getBounds2D();


        //-- Variable Declaration(s):
        double shapeX1 = 0;                     //-- Top left corner coordinates.
        double shapeY1 = 0;                     //
        double shapeX2 = 0;                     //-- Bottom right corner coordinates.
        double shapeY2 = 0;                     //

        if (reference.m_Type == PainterButton.Tool.Line)
        {
            shapeX1 = ((Line2D.Double) reference.m_Shape).getX1();
            shapeY1 = ((Line2D.Double) reference.m_Shape).getY1();
            shapeX2 = ((Line2D.Double) reference.m_Shape).getX2();;
            shapeY2 = ((Line2D.Double) reference.m_Shape).getY2();
        }
        else
        {
            shapeX1 = boundingBox.getX();
            shapeY1 = boundingBox.getY();
            shapeX2 = boundingBox.getWidth();
            shapeY2 = boundingBox.getHeight();
        }



        switch (reference.m_Type)
        {
            case Rectangle:
                reference.m_Shape = new Rectangle2D.Double(shapeX1 - xDelta, shapeY1 - yDelta, shapeX2, shapeY2);
                repaint();
                break;
            case Ellipse:
                reference.m_Shape = new Ellipse2D.Double(shapeX1 - xDelta, shapeY1 - yDelta, shapeX2, shapeY2);
                repaint();
                break;
            case Line:
                reference.m_Shape = new Line2D.Double(shapeX1 - xDelta, shapeY1 - yDelta, shapeX2- xDelta, shapeY2 - yDelta);
                break;
            default:
                break;
        }

        // Reset previous points location to current.
        m_BaseLocation = location;
    }



    /* drawHandler:
     *  Draw shape using pointer.
     * -Parameters:  Point location
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void drawHandler(Point location)
    {

        // Create base <Shape> in <SceneData>.
        if (!Objects.nonNull(m_SceneData.get(m_SceneData.size() - 1)))
        {
            m_SceneData.set(m_SceneData.size() - 1, new JSShape());
            m_SceneData.get(m_SceneData.size() - 1).m_Type = m_ActiveTool;
            m_SceneData.get(m_SceneData.size() - 1).m_ColourType = m_ActiveColourType;
            m_SceneData.get(m_SceneData.size() - 1).m_FillColour = m_ActiveFillColour;
            m_SceneData.get(m_SceneData.size() - 1).m_Thickness = m_ActiveThickness;
        }


        // Setup width and height for <Rectangle>s and <Ellipse>s.
        double width = 0;
        double height = 0;
        double startX = m_BaseLocation.getX();
        double startY = m_BaseLocation.getY();
        if (m_ActiveTool != PainterButton.Tool.Line)
        {
            width = Math.abs(m_BaseLocation.getX() - location.getX());
            height = Math.abs(m_BaseLocation.getY() - location.getY());

            startX = Math.min(startX, location.getX());
            startY = Math.min(startY, location.getY());
        }


        switch (m_ActiveTool)
        {
            case Line:
                m_SceneData.get(m_SceneData.size() - 1).m_Shape = new Line2D.Double(startX,
                                                                                    startY,
                                                                                    location.getX(),
                                                                                    location.getY());
                break;
            case Ellipse:
                m_SceneData.get(m_SceneData.size() - 1).m_Shape = new Ellipse2D.Double(startX,
                                                                                       startY,
                                                                                       width,
                                                                                       height);
                break;
            case Rectangle:
                m_SceneData.get(m_SceneData.size() - 1).m_Shape = new Rectangle2D.Double(startX,
                                                                                         startY,
                                                                                         width,
                                                                                         height);
                break;
            default:
                break;
        }
    }



    /* saveHandler:
     *  Returns a reference to the scene data array so it can be written to disk.
     * -Parameters:  n/a
     * -Returns:     ArrayList<JSShape>
     * -Throws:      n/a
     */
    public ArrayList<JSShape> saveHandler()
    {
        return m_SceneData;
    }



    /* loadHandler:
     *  Loads the scene data into <m_SceneData>.
     * -Parameters:  Object sceneData.
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void loadHandler(Object sceneData)
    {
        // Safety check:
        if (sceneData instanceof ArrayList)
        {
            // Load and redraw scene.
            m_SceneData = (ArrayList<JSShape>) sceneData;
            repaint();
        }
        else
        {
            System.err.println("LOAD ERROR: Read file does not appear to be a Painter image.");
        }

    }



    /* setClickedPoint:
     *  Loads the scene data into <m_SceneData>.
     * -Parameters:  Object sceneData.
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setClickedPoint(Point location)
    {
        m_BaseLocation = location;
    }



    /* completeShape:
     *  Loads the scene data into <m_SceneData>.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void completeShape()
    {
        int idx = m_SceneData.size() - 1;
        if (idx >= 0 && Objects.nonNull(m_SceneData.get(idx)))
        {
            m_SceneData.add(null);
        }
    }



    /* completeShape:
     *  Loads the scene data into <m_SceneData>.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public BufferedImage getCanvasImage()
    {
        // Clear selection.
        deselectShape();

        // Create buffer to store <Canvas> image.
        BufferedImage buffImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_ARGB);

        // Copy the contents of the 2D render engine to the buffer.
        this.paint(buffImage.createGraphics());

        return buffImage;
    }


    //-- Data Member(s):
    private ArrayList<JSShape> m_SceneData = null;          // All scene data shapes stored for iteration.
    private PainterButton.Tool m_ActiveTool = null;              // Currently active tool.
    private PainterButton.Thickness m_ActiveThickness = null;    // Thickness of currently active tool.
    private PainterButton.Colours m_ActiveColourType = null;     // Currently active stroke colour.
    private Color m_ActiveFillColour = null;                // Currently active fill colour.
    private Color m_ActiveStrokeColour = null;              // Currently active stroke colour.
    private ToolPalette m_ToolPalette = null;               // Reference to the <ToolPalette>
    private int m_ActiveShapeID = -1;                       // Currently selected shape.
    private Point m_BaseLocation = null;                    // Location point to work with.



    //-- Storage container for each of the shapes that define the scene.
    private class JSShape implements Serializable {


        /* JSShape:
         *  Default constructor for shapes to be drawn on <Canvas>.
         * -Parameters:  n/a
         * -Returns:     n/a
         * -Throws:      n/a
         */
        public JSShape()
        {
        }



        /* JSShape:
         *  Constructor for shapes to be drawn on <Canvas>.
         * -Parameters:  Tool type, Color colour, BorderThickness thickness, int x, int y, int height, int width
         * -Returns:     n/a
         * -Throws:      n/a
         */
        public JSShape(PainterButton.Tool type,
                       PainterButton.Colours colourType,
                       Color colour,
                       PainterButton.Thickness thickness,
                       double x,
                       double y,
                       double height,
                       double width)
        {

            // Instantiate data members.
            m_Type = type;
            m_ColourType = colourType;
            m_FillColour = colour;
            m_Thickness = thickness;


            // Select the right drawing tool.
            switch (m_Type)
            {
                case Line:
                    m_Shape = new Line2D.Double(x, y, width, height);
                    break;
                case Rectangle:
                    m_Shape = new Rectangle2D.Double(x, y, height, width);
                    break;
                case Ellipse:
                    m_Shape = new Ellipse2D.Double(x, y, height, width);
                    break;
                default:
                    break;
            }
        }



        //-- Data Member(s):
        public Shape m_Shape = null;                    // Shape object to draw on <Canvas>.
        public PainterButton.Tool m_Type = null;             // Type of <Shape> to draw on <Canvas>.
        public PainterButton.Colours m_ColourType = null;    // Tool Palette colour type.
        public Color m_FillColour = null;               // Colour of <Shape> to be drawn on <Canvas>.
        public PainterButton.Thickness m_Thickness;          // Thickness of shape lines.
        private static final long serialVersionUID = 1L;// Serial check to ensure file is correct type.
    }
}
