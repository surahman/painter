            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Painter.java                    #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                Java Painter Class.                #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Painter {

    /* Painter:
     *  Constructor for the basic paint application.
     * -Parameters:  int width, int height
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Painter (int width, int height)
    {
        // Setup resource directories and window sizes. TWEAK HERE AS NEEDED.
        k_AppIconsDir = "/app/";
        k_ToolIconsDir = "/tools/";
        k_MenuIconsDir = "/menu/";
        k_GUIDims = new Dimension(width, height);
        k_ToolDims = new Dimension(130, 600);
        k_CanvasDims = new Dimension(k_GUIDims.width - k_ToolDims.width,
                                     k_GUIDims.height - k_ToolDims.height);


        // Setup Application Icons.
        k_AppIcons = PainterButton.LoadAppIcon(k_AppIconsDir);


        // Setup the <FileChooser> dialog.
        k_FileExt = "painter";
        k_FileChooser = new JFileChooser();
        k_FileChooser.setMultiSelectionEnabled(false);
        k_FileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        k_FileChooser.setFileFilter(new FileNameExtensionFilter("Painter Image", k_FileExt));


        // Create tool palette.
        k_ToolPalette = new ToolPalette(k_ToolDims.width, k_ToolDims.height, k_ToolIconsDir);


        // Create canvas.
        k_CanvasPanel = new Canvas(k_CanvasDims);


        // Cross register objects.
        k_CanvasPanel.registerToolPalette(k_ToolPalette);
        k_ToolPalette.registerCanvas(k_CanvasPanel);


        // Create new GUI thread to be launched by event-dispatcher thread.
        SwingUtilities.invokeLater (
            new Runnable()
            {
                public void run()
                {
                    DrawMainWindow(width, height);
                }
            }
        );
    }



    /* DrawMainWindow:
     *  Draw the main GUI window.
     * -Parameters:  int width, int height
     * -Returns:     n/a
     * -Throws:      NullPointerException
     */
    private void DrawMainWindow(int width, int height)
    {
        // Setup GUI parameters.
        m_GUI = new JFrame("Painter");
        m_GUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        m_GUI.setSize(width, height);
        m_GUI.setResizable(false);
        m_GUI.setLocationRelativeTo(null);
        m_GUI.setVisible(true);
        m_GUI.setIconImages(k_AppIcons);


        // Setup Main panel.
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        m_GUI.add(mainPanel);


        // Setup Tool Palette panel.
        mainPanel.add(k_ToolPalette, BorderLayout.WEST);


        // Setup Canvas panel.
        mainPanel.add(k_CanvasPanel, BorderLayout.CENTER);


        // Initialize menu bar.
        createMainMenu();
    }



    /* createMainMenu:
     *  Create the main menu bar.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      NullPointerException
     */
    private void createMainMenu()
    {
        // Safety Check(s): Ensure the main GUI has been initialized.
        if (m_GUI == null)
        {
            throw new NullPointerException("ERROR: Main GUI <m_GUI> not initialized.");
        }


        // Create <File> menu.
        JMenu fileMenu = new JMenu("File");

        // Exit menu icon from resources.
        String iconName = "exit_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        // Exit menu item with close action linked.
        JMenuItem exit = new JMenuItem("   Exit", iconFile);
        exit.setToolTipText("Close Painter.");
        exit.addActionListener((event) -> System.exit(0));


        // Add <File> menu items.
        fileMenu.addSeparator();
        fileMenu.add(createNewItem());
        fileMenu.add(createOpenItem());
        fileMenu.add(createSaveItem());
        fileMenu.addSeparator();
        fileMenu.add(exit);
        fileMenu.addSeparator();


        // Create <Edit> menu.
        JMenu editMenu = new JMenu("Edit");
        editMenu.add(createCopyItem());

        // Create <Help> menu.
        JMenu helpMenu = new JMenu("Help");
        helpMenu.add(openReadme());
        helpMenu.add(createAboutDialog());


        // Create main menu bar area.
        JMenuBar mainMenuBar = new JMenuBar();
        mainMenuBar.add(fileMenu);
        mainMenuBar.add(editMenu);
        mainMenuBar.add(helpMenu);

        // Initialize the main menu bar.
        m_GUI.setJMenuBar(mainMenuBar);
    }



    /* createAboutDialog:
     *  Create the About dialog window.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem createAboutDialog()
    {
        // Create about dialog box.
        ImageIcon PainterAbout = PainterButton.LoadImageIcon(k_AppIconsDir, "24px.png");
        JMenuItem aboutMenu = new JMenuItem("   About", PainterAbout);
        aboutMenu.setToolTipText("About Painter.");
        ImageIcon PainterLogo = PainterButton.LoadImageIcon(k_AppIconsDir, "128px.png");

        // Text area for dialog box.
        JLabel textArea = new JLabel (
                "<html>" +
                "<h1 style=\"text-align: center;\"><span style=\"text-decoration: underline;\">" +
                "<strong>Painter</strong></span></h1>" +
                "<ul>" +
                "<li>Construct an interface using toolkit widgets and layouts (Swing/AWT).</li>" +
                "<li>Support interaction with a canvas, where users can draw shapes using graphics primitives.</li>" +
                "<li>Handle hit detection for arbitrary shapes.</li>" +
                "</ul><br>" +
                "<p style=\"text-align: center;\"><em>&copy; Saad Ur Rahman. All rights reserved.</em></p>" +
                "<p style=\"text-align: center;\"><em>Usage and source code is licensed under the GNU Affero General " +
                "Public License v3.0.</em></p>" +
                "<p style=\"text-align: center;\"><em><sub>Icons made by www.Freepik.com from www.FlatIcon.com" +
                " are licensed by Creative Commons BY 3.0.</sub></em></p>" +
                "</html>"
        );


        // About dialog box setup.
        aboutMenu.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    JOptionPane.showMessageDialog(
                            null,
                            textArea,
                            "About Painter",
                            JOptionPane.PLAIN_MESSAGE,
                            PainterLogo);
                }
            }
        );


        return aboutMenu;
    }



    /* createAboutDialog:
     *  Create the About dialog window.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem openReadme()
    {
        // Readme menu icon from resources.
        String iconName = "readme_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        JMenuItem button = new JMenuItem("   Readme", iconFile);
        button.setToolTipText("Open readme.txt file in text editor.");

        // Attempt to open readme file.
        button.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    try
                    {
                        Desktop openReadme = Desktop.getDesktop();
                        openReadme.open(new File("README.txt"));
                    }
                    catch (IOException except)
                    {
                        System.out.println("ERROR: Failed to locate <README.txt>.");
                    }
                }
            }
        );

        return button;
    }



    /* createCopyItem:
     *  Create <Copy> menu item.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem createCopyItem()
    {
        // Copy menu icon from resources.
        String iconName = "copy_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        JMenuItem button = new JMenuItem("   Copy", iconFile);
        button.setToolTipText("Copy complete canvas as image to system clipboard.");

        // Attempt to open a file.
        button.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {

                    // Setup hooks to system clipboard and copy over <BufferedImage>.
                    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                    xFerImage trans = new xFerImage(k_CanvasPanel.getCanvasImage());
                    clipboard.setContents(trans, null);
                }
            }
        );

        return button;
    }



    /* createNewItem:
     *  Create <New> menu item.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem createNewItem()
    {
        // New Document menu icon from resources.
        String iconName = "new-file_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        JMenuItem button = new JMenuItem("   New", iconFile);
        button.setToolTipText("Create new document.");

        // Attempt to open a file.
        button.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    k_CanvasPanel.clear();
                }
            }
        );


        return button;
    }



    /* createOpenItem:
     *  Create <Open> menu item.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem createOpenItem()
    {
        // Open Document menu icon from resources.
        String iconName = "folder_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        JMenuItem button = new JMenuItem("   Open", iconFile);
        button.setToolTipText("Open document from disk.");

        // Attempt to open a file.
        button.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {

                    // Setup file open dialog window name.
                    k_FileChooser.setDialogTitle("Open file");

                    // Attempt to open file.
                    int retval = k_FileChooser.showDialog(null, "Open");
                    if (retval == JFileChooser.APPROVE_OPTION)
                    {
                        /* Guide to how Object file save/load can be achieved is from:
                         * https://www.mkyong.com/java/how-to-write-an-object-to-file-in-java/
                         * https://stackoverflow.com/questions/17293991/how-to-write-and-read-java-serialized-objects-into-a-file
                         */

                        // Setup file descriptor to read from.
                        File fd = k_FileChooser.getSelectedFile();

                        // Open file and read in data.
                        try
                        {
                            ObjectInputStream objStream = new ObjectInputStream(new FileInputStream(fd));
                            k_CanvasPanel.loadHandler(objStream.readObject());
                            objStream.close();
                        }
                        catch (Exception except)
                        {
                            System.out.println("ERROR: " + except.getMessage());
                        }
                    }
                    else
                    {
                        System.out.println("Aborted file open operation.");
                    }
                }
            }
        );


        return button;
    }



    /* createSaveItem:
     *  Create <Save> menu item.
     * -Parameters:  n/a
     * -Returns:     JMenuItem
     * -Throws:      n/a
     */
    private JMenuItem createSaveItem()
    {
        // Save Document menu icon from resources.
        String iconName = "save_24px.png";
        ImageIcon iconFile = PainterButton.LoadImageIcon(k_MenuIconsDir, iconName);

        JMenuItem button = new JMenuItem("   Save", iconFile);
        button.setToolTipText("Save document to disk.");

        // Attempt to open a file.
        button.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {

                    // Setup file open dialog window name.
                    k_FileChooser.setDialogTitle("Save file");

                    // Attempt to open file.
                    int retval = k_FileChooser.showDialog(null, "Save");
                    if (retval == JFileChooser.APPROVE_OPTION)
                    {
                        /* Guide to how Object file save/load can be achieved is from:
                         * https://www.mkyong.com/java/how-to-write-an-object-to-file-in-java/
                         * https://stackoverflow.com/questions/17293991/how-to-write-and-read-java-serialized-objects-into-a-file
                         */

                        // Setup file descriptor to write too.
                        String fileName = k_FileChooser.getSelectedFile().getPath() + "." + k_FileExt;
                        File fd = new File(fileName);

                        // Open file and write out raw object data.
                        try
                        {

                            ObjectOutputStream objStream = new ObjectOutputStream(new FileOutputStream(fd));
                            objStream.writeObject(k_CanvasPanel.saveHandler());
                            objStream.close();

                        }
                        catch (Exception except)
                        {
                            System.out.println("ERROR: " + except.getMessage());
                        }
                    }
                }
            }
        );



        return button;
    }




    //-- Data Member(s):
    private final String k_AppIconsDir;         // Main GUI icons directory.
    private final String k_ToolIconsDir;        // Tools palette icons directory.
    private final String k_MenuIconsDir;        // Main menu icons directory.
    private final Dimension k_GUIDims;          // Main window dimensions.
    private final Dimension k_ToolDims;         // Main tool panel dimensions.
    private final Dimension k_CanvasDims;       // Main canvas panel dimensions.
    private final ToolPalette k_ToolPalette;    // Main tool palette.
    private final Canvas k_CanvasPanel;         // Main canvas panel.
    private final List<BufferedImage> k_AppIcons;// Application icons for GUI windows.
    private final JFileChooser k_FileChooser;   // Frame work to browse and open files.
    private final String k_FileExt;             // File name extension.
    private JFrame m_GUI = null;                // Main GUI window.



    //-- <Transferable> class to support image transfers.
    private class xFerImage implements Transferable
    {


        /* xFerImage:
         *  Constructor to set data member.
         * -Parameters:  Image image
         * -Returns:     n/a
         * -Throws:      n/a
         */
        private xFerImage(Image image)
        {
            m_Image = image;
        }



        /* getTransferDataFlavors:
         *  Returns the type of data stored in this container.
         * -Parameters:  n/a
         * -Returns:     DataFlavor[]
         * -Throws:      n/a
         */
        public DataFlavor[] getTransferDataFlavors()
        {
            DataFlavor[] dataFlavour = { DataFlavor.imageFlavor };
            return dataFlavour;
        }



        /* isDataFlavorSupported:
         *  Checks if the requested data type is an image.
         * -Parameters:  DataFlavor flavour
         * -Returns:     boolean
         * -Throws:      n/a
         */
        public boolean isDataFlavorSupported(DataFlavor flavour)
        {
            return flavour.equals(DataFlavor.imageFlavor);
        }



        /* getTransferData:
         *  Checks if the requested data type is an image, if so returns <Image>.
         * -Parameters:  DataFlavor flavour
         * -Returns:     Object
         * -Throws:      UnsupportedFlavorException
         */
        public Object getTransferData(DataFlavor flavour) throws UnsupportedFlavorException
        {
            if (flavour.equals(DataFlavor.imageFlavor))
            {
                return m_Image;
            }
            else
            {
                throw new UnsupportedFlavorException(flavour);
            }
        }



        //-- Data Method(s):
        private Image m_Image;
    }
}
