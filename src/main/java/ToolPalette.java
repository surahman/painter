            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ToolPalette.java                #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #         Java Sketch Tool Palette Class.           #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/


public class ToolPalette extends JPanel{

    /* ToolPalette:
     *  Constructor for the tool panel.
     * -Parameters:  int width, int height, String ToolIconDir
     * -Returns:     n/a
     * -Throws:      n/a
     */
    ToolPalette(int width, int height, String ToolIconDir)
    {
        // Setup resource directories.
        k_ToolIconDir = ToolIconDir;

        // Set height and width of panel.
        k_Height = height;
        k_Width = width;

        // Setup global colour for all the tool palette background.
        k_BackgroundColour = Color.LIGHT_GRAY;

        // Setup border style for panels.
        k_etchedStyleBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

        // Setup Button Groups.
        m_ToolGroup = new ButtonGroup();
        m_ColourGroup = new ButtonGroup();
        m_ThicknessGroup = new ButtonGroup();

        // Register all the button groups with button classes.
        PainterButton.SetupGroups(m_ToolGroup, m_ColourGroup, m_ThicknessGroup);

        // Setup main tool panel.
        SetupMainPanel();
    }



    /* registerCanvas:
     *  Create reference to <Canvas> object and setup the <Tool Panel>, <Colour Panel>, and <Thickness Panel>.
     * -Parameters:  Canvas canvas
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void registerCanvas(Canvas canvas)
    {
        // Setup reference to the canvas.
        m_Canvas = canvas;

        // Add tool panel.
        SetupToolPanel();

        // Add color panel.
        SetupColourPanel();

        // Add tool thickness panel.
        SetupThicknessPanel();
    }



    /* SetupMainPanel:
     *  Setup <MainPanel>.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void SetupMainPanel()
    {
        // Setup properties.
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.setBackground(k_BackgroundColour);

        Dimension toolDim = new Dimension(k_Width, k_Height);
        this.setPreferredSize(toolDim);
        this.setMaximumSize(toolDim);
        this.setMinimumSize(toolDim);
    }



    /* SetupToolPanel:
     *  Draw the main tool panel.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void SetupToolPanel()
    {
        // Setup main panel to house Tool/Color/Line panels.
        m_ToolButtons = new ToolButton[PainterButton.Tool.values().length];

        // Setup tool panel.
        m_ToolPanel = new JPanel();
        m_ToolPanel.setLayout(new GridLayout(3, 2, 1, 1));
        m_ToolPanel.setBackground(k_BackgroundColour);
        m_ToolPanel.setMaximumSize(new Dimension(110, 170));
        TitledBorder panelLabel = BorderFactory.createTitledBorder(k_etchedStyleBorder, "Tools");
        panelLabel.setTitleJustification(TitledBorder.LEFT);
        m_ToolPanel.setBorder(panelLabel);

        // Setup <Select> button.
        m_ToolButtons[PainterButton.Tool.Select.ordinal()] = new ToolButton( m_Canvas,
                                                                        k_ToolIconDir,
                                                                "select_colour_32px.png",
                                                                        PainterButton.Tool.Select);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Select.ordinal()]);

        // Setup <Erase> button.
        m_ToolButtons[PainterButton.Tool.Eraser.ordinal()] = new ToolButton( m_Canvas,
                                                                        k_ToolIconDir,
                                                                "eraser_colour_32px.png",
                                                                        PainterButton.Tool.Eraser);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Eraser.ordinal()]);

        // Setup <Line> button.
        m_ToolButtons[PainterButton.Tool.Line.ordinal()] = new ToolButton(   m_Canvas,
                                                                        k_ToolIconDir,
                                                                "line_colour_32px.png",
                                                                        PainterButton.Tool.Line);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Line.ordinal()]);

        // Setup <Circle> button.
        m_ToolButtons[PainterButton.Tool.Ellipse.ordinal()] = new ToolButton(    m_Canvas,
                                                                            k_ToolIconDir,
                                                                    "circle_colour_32px.png",
                                                                            PainterButton.Tool.Ellipse);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Ellipse.ordinal()]);

        // Setup <Rectangle> button.
        m_ToolButtons[PainterButton.Tool.Rectangle.ordinal()] = new ToolButton(  m_Canvas,
                                                                            k_ToolIconDir,
                                                                    "rectangle_colour_32px.png",
                                                                            PainterButton.Tool.Rectangle);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Rectangle.ordinal()]);

        // Setup <Fill> button.
        m_ToolButtons[PainterButton.Tool.Fill.ordinal()] = new ToolButton(   m_Canvas,
                                                                        k_ToolIconDir,
                                                                "fill_colour_32px.png",
                                                                        PainterButton.Tool.Fill);
        m_ToolPanel.add(m_ToolButtons[PainterButton.Tool.Fill.ordinal()]);

        // Setup default tool.
        m_ToolGroup.setSelected(m_ToolButtons[PainterButton.Tool.Line.ordinal()].getModel(), true);

        // Add the Tool Panel to the main panel.
        this.add(m_ToolPanel);
    }



    /* SetupColourPanel:
     *  Draw the main colour panel.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void SetupColourPanel()
    {
        // Set up color swatch panel.
        m_ColourButtons = new JToggleButton[PainterButton.Colours.values().length];

        // Setup colour panel.
        m_ColourPanel = new JPanel();
        m_ColourPanel.setLayout(new GridLayout(4, 2, 3,3));
        m_ColourPanel.setBackground(k_BackgroundColour);
        m_ColourPanel.setMaximumSize(new Dimension(110, 180));
        TitledBorder panelLabel = BorderFactory.createTitledBorder(k_etchedStyleBorder, "Colours");
        panelLabel.setTitleJustification(TitledBorder.LEFT);
        m_ColourPanel.setBorder(panelLabel);

        // Setup icon for selected colour.
        Icon selectedIcon = PainterButton.LoadIcon(k_ToolIconDir, "colour_selected.png");
        Icon transparentIcon = PainterButton.LoadIcon(k_ToolIconDir, "transparent_square.png");


        // Insert <BLACK> color.
        m_ColourButtons[PainterButton.Colours.Black.ordinal()] = new ColourButton(m_Canvas,
                                                                             Color.BLACK,
                                                                             PainterButton.Colours.Black,
                                                                             selectedIcon,
                                                                             transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Black.ordinal()]);

        // Insert <PINK> color.
        m_ColourButtons[PainterButton.Colours.Pink.ordinal()] = new ColourButton(m_Canvas,
                                                                            Color.PINK,
                                                                            PainterButton.Colours.Pink,
                                                                            selectedIcon,
                                                                            transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Pink.ordinal()]);

        // Insert <RED> color.
        m_ColourButtons[PainterButton.Colours.Red.ordinal()] = new ColourButton(m_Canvas,
                                                                           Color.RED,
                                                                           PainterButton.Colours.Red,
                                                                           selectedIcon,
                                                                           transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Red.ordinal()]);

        // Insert <GREEN> color.
        m_ColourButtons[PainterButton.Colours.Green.ordinal()] = new ColourButton(m_Canvas,
                                                                             Color.GREEN,
                                                                             PainterButton.Colours.Green,
                                                                             selectedIcon,
                                                                             transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Green.ordinal()]);

        // Insert <BLUE> color.
        m_ColourButtons[PainterButton.Colours.Blue.ordinal()] = new ColourButton(m_Canvas,
                                                                            Color.BLUE,
                                                                            PainterButton.Colours.Blue,
                                                                            selectedIcon,
                                                                            transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Blue.ordinal()]);

        // Insert <YELLOW> color.
        m_ColourButtons[PainterButton.Colours.Yellow.ordinal()] = new ColourButton(m_Canvas,
                                                                              Color.YELLOW,
                                                                              PainterButton.Colours.Yellow,
                                                                              selectedIcon,
                                                                              transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Yellow.ordinal()]);

        // Insert <CYAN> color.
        m_ColourButtons[PainterButton.Colours.Cyan.ordinal()] = new ColourButton(m_Canvas,
                                                                            Color.CYAN,
                                                                            PainterButton.Colours.Cyan,
                                                                            selectedIcon,
                                                                            transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Cyan.ordinal()]);

        // Insert <MAGENTA> color.
        m_ColourButtons[PainterButton.Colours.Magenta.ordinal()] = new ColourButton(m_Canvas,
                                                                               Color.MAGENTA,
                                                                               PainterButton.Colours.Magenta,
                                                                               selectedIcon,
                                                                               transparentIcon);
        m_ColourPanel.add(m_ColourButtons[PainterButton.Colours.Magenta.ordinal()]);

        // Setup default colour.
        m_ColourGroup.setSelected(m_ColourButtons[PainterButton.Colours.Black.ordinal()].getModel(), true);

        // Add Colour panel to the main tool panel.
        this.add(m_ColourPanel);

        // Setup and add custom colour selector.
        CreateColourSelector(selectedIcon, transparentIcon);
    }



    /* CreateColourSelector:
     *  Create custom colour selector button.
     * -Parameters:  Icon selectedIcon, Icon deselectedIcon
     * -Returns:     JPanel
     * -Throws:      n/a
     */
    private void CreateColourSelector(Icon selectedIcon, Icon deselectedIcon)
    {
        // Setup panel and layout.
        JPanel colourSelector = new JPanel();
        colourSelector.setBackground(k_BackgroundColour);
        colourSelector.setLayout(new GridLayout(1,1));
        colourSelector.setMaximumSize(new Dimension(110, 60));

        // Setup panel description label.
        TitledBorder panelLabel = BorderFactory.createTitledBorder(k_etchedStyleBorder, "Custom Colour:");
        panelLabel.setTitleJustification(TitledBorder.LEFT);
        colourSelector.setBorder(panelLabel);

        // Setup selector swatch button.
        m_ColourButtons[PainterButton.Colours.Custom.ordinal()] = new ColourSelectorButton(  m_Canvas,
                                                                                        k_BackgroundColour,
                                                                                        PainterButton.Colours.Custom,
                                                                                        selectedIcon,
                                                                                        deselectedIcon);


        // Setup the JPanel elements.
        colourSelector.add(m_ColourButtons[PainterButton.Colours.Custom.ordinal()]);


        // Add the panel to the main group.
        this.add(colourSelector);
    }



    /* SetupColourPanel:
     *  Draw the main colour panel.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void SetupThicknessPanel()
    {
        // Set up thickness button groups.
        m_ThicknessButtons = new JToggleButton[PainterButton.Thickness.values().length];

        // Set up thickness panel.
        m_ThicknessPanel = new JPanel();
        m_ThicknessPanel.setBackground(k_BackgroundColour);
        m_ThicknessPanel.setLayout(new GridLayout(4, 1, 0,0));
        m_ThicknessPanel.setMaximumSize(new Dimension(110, 200));


        // Setup panel description label.
        TitledBorder panelLabel = BorderFactory.createTitledBorder(k_etchedStyleBorder, "Thickness:");
        panelLabel.setTitleJustification(TitledBorder.LEFT);
        m_ThicknessPanel.setBorder(panelLabel);


        // Load panel images.
        Icon px5 = PainterButton.LoadIcon(k_ToolIconDir, "thickness_5px.png");
        Icon px10 = PainterButton.LoadIcon(k_ToolIconDir, "thickness_10px.png");
        Icon px20 = PainterButton.LoadIcon(k_ToolIconDir, "thickness_20px.png");
        Icon px30 = PainterButton.LoadIcon(k_ToolIconDir, "thickness_30px.png");

        // Insert <10px> thickness.
        m_ThicknessButtons[PainterButton.Thickness.Thin.ordinal()] = new ThicknessButton(    m_Canvas,
                                                                                        k_BackgroundColour,
                                                                                        px5,
                                                                                        PainterButton.Thickness.Thin);
        m_ThicknessPanel.add(m_ThicknessButtons[PainterButton.Thickness.Thin.ordinal()]);

        // Insert <20px> thickness.
        m_ThicknessButtons[PainterButton.Thickness.Normal.ordinal()] = new ThicknessButton(  m_Canvas,
                                                                                        k_BackgroundColour,
                                                                                        px10,
                                                                                        PainterButton.Thickness.Normal);
        m_ThicknessPanel.add(m_ThicknessButtons[PainterButton.Thickness.Normal.ordinal()]);

        // Insert <30px> thickness.
        m_ThicknessButtons[PainterButton.Thickness.Thick.ordinal()] = new ThicknessButton(   m_Canvas,
                                                                                        k_BackgroundColour,
                                                                                        px20,
                                                                                        PainterButton.Thickness.Thick);
        m_ThicknessPanel.add(m_ThicknessButtons[PainterButton.Thickness.Thick.ordinal()]);

        // Insert <40px> thickness.
        m_ThicknessButtons[PainterButton.Thickness.Thicker.ordinal()] = new ThicknessButton( m_Canvas,
                                                                                        k_BackgroundColour,
                                                                                        px30,
                                                                                        PainterButton.Thickness.Thicker);
        m_ThicknessPanel.add(m_ThicknessButtons[PainterButton.Thickness.Thicker.ordinal()]);


        // Set Default tool.
        m_ThicknessGroup.setSelected(m_ThicknessButtons[PainterButton.Thickness.Normal.ordinal()].getModel(), true);
        
        // Add Tool Thickness panel.
        this.add(m_ThicknessPanel);
    }



    /* notify:
     *  Set the selected <Shape>s colour and thickness for the tool palette.
     * -Parameters:  PainterButton.Thickness selectedThickness, PainterButton.Colours selectedColour, Color customColor
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void notify(PainterButton.Thickness selectedThickness,
                       PainterButton.Colours selectedColour,
                       Color customColor)
    {
        m_ColourButtons[selectedColour.ordinal()].setSelected(true);
        m_ThicknessButtons[selectedThickness.ordinal()].setSelected(true);

        // Setup custom colours state.
        if (selectedColour == PainterButton.Colours.Custom && customColor != null)
        {
            m_ColourButtons[PainterButton.Colours.Custom.ordinal()].setBackground(customColor);
        }
    }




    //-- Data Member(s):
    private final String k_ToolIconDir;                 // Tools palette icon directory.
    private final int k_Height;                         // Maximum height of panel.
    private final int k_Width;                          // Maximum width of panel.
    private final Border k_etchedStyleBorder;           // Lowered-etched style of border.
    private final Color k_BackgroundColour;             // Global panel background colour.
    private JPanel m_ToolPanel = null;                  // Main tool palette panel.
    private JPanel m_ColourPanel = null;                // Main color palette panel.
    private JPanel m_ThicknessPanel = null;             // Main line thickness palette panel.
    private ButtonGroup m_ToolGroup = null;             // Tool panel button group.
    private ButtonGroup m_ColourGroup = null;           // Colour panel button group.
    private ButtonGroup m_ThicknessGroup = null;        // Thickness panel button group.
    private ToolButton[] m_ToolButtons = null;          // All tool buttons.
    private JToggleButton[] m_ColourButtons = null;     // All colour buttons.
    private JToggleButton[] m_ThicknessButtons = null;  // All line thickness buttons.
    private Canvas m_Canvas = null;                     // Reference to <Canvas> object.
}
