            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    ColourButton.java               #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Colour JToggleButton Class.            #
            #__________________________________________________*/



//- Import(s): Library:
import javax.swing.*;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/


public class ColourButton extends PainterButton {



    /* ColourButton:
     *  Colour panel button constructor.
     * -Parameters:  Color colourName, PainterButton.Colours colourID, Icon selected, Icon deselected
     * -Returns:     n/a
     * -Throws:      n/a
     */
    ColourButton(Canvas canvas, Color colourName, PainterButton.Colours colourID, Icon selected, Icon deselected)
    {
        // Setup <button> parameters.
        this.setPreferredSize(new Dimension(32, 32));
        this.setBackground(colourName);
        this.setBorderPainted(true);
        this.setIcon(deselected);
        this.setSelectedIcon(selected);

        // Set buttons depressed state colour.
        this.setUI(
            new MetalToggleButtonUI()
            {
                @Override
                protected Color getSelectColor()
                {
                    return colourName;
                }
            }
        );

        // Setup the colour group.
        m_ColourGroup.add(this);


        // On-click update colour.
        this.addActionListener(
            new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {

                    // Update the <Canvas> colour.
                    canvas.setColour(colourName, colourID);
                }
            }
        );
    }

}
