
<p align="center" width="100%">
    <img src="./src/main/resources/app/256px.png">
</p>

## Java Painter

[ License ] |
:---------: |

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `LICENSE` in the root directory of this project and can also be found online
<a href="https://opensource.org/licenses/AGPL-3.0" title="GNU Affero General Public License v3.0">here</a>.


<br>
<br>

[ System Specs ] |
:---------: |

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the toolsets shouldn't break the build.

|Software| Version
|:------------: | :-------------:|
Java Runtime Environment | build 1.8.0_212-b10 x64
Java Development Kit | build 1.8.0_212 x64
Gradle | 5.4.1
Windows 10 Pro | 1903 x64

<br>
<br>

|[ Usage & Build ] |
|:---------: |

[![pipeline status](https://gitlab.com/surahman/painter/badges/main/pipeline.svg)](https://gitlab.com/surahman/painter/-/commits/main)

- [X] Executing `gradle build` will build *Painter*. You can find the generated fat jar file in `/build/libs`; please
ensure that the `README.txt` and `LICENSE` files are in the same directory as the fat jar file when you run it.
- [x] Executing `gradle run` will run *Painter*. Note that is not necessary to run the `build` command above; you can
simply execute the `run` command to build and start the application.
- [x] Included in the root directory of this project is the archive `Painter-1.1.zip` which has a self-contained fat jar
 file. To run the application simply extract the contents of the archive, open a command prompt and navigate to where
 you extracted the files. Running the command `java -jar Painter-1.1.jar` will launch the application.

<br>
<br>

|[ Features ] |
|:---------:|


###### Screen Shots:
Screenshots are located <a href="screenshots/" title="screenshots">here</a>.
<p align="center" width="100%">
    <img src="./screenshots/screenshot_001.png" width="50%" height="50%">
</p>


##### File Operations ![File Operations](./src/main/resources/app/16px.png)
The data written to a file from the canvas is written as a raw Java `Object` format and is thus not human readable. To
write the file to disk *Painter* is using `Object Serializable` interface, so every time the application is recompiled
any files written with an "older" version of the application becomes unreadable by the new build. Thus any file that is
Loaded must be Saved with the same build.

- [x] ![New](./src/main/resources/menu/new-file_24px.png) New: This will clear the scene and create a blank canvas.
- [x] ![Open](./src/main/resources/menu/folder_24px.png) Open: This will open a saved `.painter` file created with the
same build.
- [x] ![Save](./src/main/resources/menu/save_24px.png) Save: This will save a `.painter` file using raw `Serialized`
scene data.

<br/>
<br/>

##### Tool Palette ![Tool Palette](./src/main/resources/app/16px.png)

###### Select Tool ![Select Tool](./src/main/resources/tools/select_colour_32px.png)
- [x] Select shape: Clicking on a shape will bring it to the top of all of the other shapes on the canvas, and this is
by design. It isn't efficient as the underlying data structure containing the scene data is an `ArrayList` and
performing this action requires shifting the array elements down to fill in the gap created. I felt it was essential to
allow shapes to be reordered on the screen.
- [x] Deselect shape: Click anywhere on the canvas, or another shape, and it will deselect the shape and return it to
the solid black border.
- [x] ESC: Hitting the `ESC` key will deselect the currently selected shape.
- [x] Canvas Indication: Selecting a shape will bring it to the top of all other shapes but also switch its border to a
dashed line which will change thickness based on the actual border thickness applied to the shape.
- [x] Tool Palette Indication: The colour and thickness of the shape selected will update the respective swatches in
the tool palette.
- [x] Change Border: Selecting a shape and adjusting the thickness will change the thickness to the one selected. The
on screen shape will have a dashed border to indicate selection with a border thickness corresponding to the thickness
selected.
- [x] Change Colour: Selecting a shape and clicking on a colour swatch will update the colour of the shape.
- [x] Moving: Selecting a shape and dragging will move it. A shape can only be moved if it has been clicked on and
selected first, after which you can click and drag anywhere on the screen and it will move in the corresponding
direction and distance.
- [x] Live View: The movements, colour, and thickness will be updated on the canvas in realtime.


###### Eraser Tool ![Erase Tool](./src/main/resources/tools/eraser_colour_32px.png)
- [x] Select: With the tool active click on a shape to remove it from the scene.


###### Line Tool ![Line Tool](./src/main/resources/tools/line_colour_32px.png)
- [x] Draw: Click on a start point and drag in any direction to create a line in that direction, release the mouse to
complete the line in the colour and thickness selected.
- [x] Live View: The line will be updated on the canvas in realtime as it's being drawn.


###### Ellipse Tool ![Ellipse Tool](./src/main/resources/tools/circle_colour_32px.png)
- [x] Draw: Click on a start point and drag in any direction to create an ellipse in that direction, release the mouse
to complete the ellipse in the colour and thickness selected.
- [x] Live View: The ellipse will be updated on the canvas in realtime as it's being drawn.


###### Rectangle Tool ![Rectangle Tool](./src/main/resources/tools/rectangle_colour_32px.png)
- [x] Draw: Click on a start point and drag in any direction to create a rectangle in that direction, release the mouse
to complete the rectangle in the colour and thickness selected.
- [x] Live View: The rectangle will be updated on the canvas in realtime as it's being drawn.


###### Fill Tool ![Fill Tool](./src/main/resources/tools/fill_colour_32px.png)
- [x] Fill: Click on a shape to fill the shape under the mouse pointer with the selected colour.
- [x] Live View: The colour fill should be updated on the canvas in realtime as it's being executed.
- [x] Line: A line should not be a fillable object. The only way to change a lines colour remains via the `Select Tool`.


<br/>
<br/>

##### Colour Palette ![Colour](./src/main/resources/app/16px.png)
- [x] Predefined: Selecting a colour from the predefined colour swatch will set the line or shapes colour when it’s
drawn, selected or has a fill applied.
- [x] Custom: Clicking on the swatch will open a colour selector dialog allowing the user to select a custom colour.
On successful selection the swatch and the active tool colour will be set to the colour.


<br/>
<br/>

##### Thickness Palette ![Thickness](./src/main/resources/app/16px.png)
- [x] Thickness: Allows the user to switch between four predefined border line thicknesses of 2px, 5px, 10px and 15px.


<br/>
<br/>

##### Extra Feature(s) ![Extra Feature(s)](./src/main/resources/app/16px.png)
- [x] ![Copy](./src/main/resources/menu/copy_24px.png) Copy: Copies the active canvas as an ARGB colour definition image
 to the system level clipboard so it can be pasted into another application. Access `Copy` fom the `Edit` menu in the
 main toolbar.


---
#### *__Development Log__*

---
<br/>

Projects were initially uploaded to a private development repository where it was developed using the Git Flow paradigm.
This repository was created to make this project public and package it in a `fat jar` file.

<br/>


##### [ May 8th 2019 ]
> * Initial project workspace setup.
> * Application takes window size as parameters with max windows resolution set as 1600*1200 (width * height).
> * Created basic main GUI window in thread to be run by event-dispatcher for thread safety.
> * Setup main GUI window to be non-resizable.
> * Setup main GUI window default exit event to close window.
> * Added application icons, see notice below.

<br/>

##### [ May 9th 2019 ]
> * Added main menu bar.
> * Added _About_ menu with icon and text.
> * Added _File_ menu item.
> * Added _Exit_ menu item under _File_ and linked it to exit action.
> * Added _New_ menu item, needs to be moved out to separate member function.
> * Added _Open_ menu item, needs to be moved out to separate member function.
> * Added _Close_ menu item, needs to be moved out to separate member function.

<br/>

##### [ May 19th 2019 ]
> * Created __*floating*__ tool panel main GUI window.
> * Tool panel is always on top of all windows.
> * Added buttons for all tools.
> * Changed selection tool icon.
> * Basic colours: _Black,
                   Pink,
                   Red,
                   Green,
                   Blue,
                   Yellow,
                   Cyan,
                   Magenta._


<br/>

##### [ May 22nd 2019 ]
> * Began work on the MVC model for _Viewport_ and _Canvas_.
> * Switched tool palette buttons to _*JToggleButton*_.
> * Set depressed button colour to _orange_.
> * Fixed Gradle script to include _main_ class name.


<br/>

##### [ May 25th 2019 ]
> * Got rid of MVC model for _Viewport_ and _Canvas_ in favour of simpler setup.
> * Added buttons for colour swatch, need to add custom colour selector.
> * Completed _ButtonGroup_ for selected objects in tool panel subsets.
> * Completed _ButtonGroup_ for selected objects in colour panel subsets.
> * Set default items in _colour_ and _tool_ groups.
> * Completed custom colour selector panel.


<br/>

##### [ May 26th 2019 ]
> * Working on _Canvas_ class constructor and essential data members.
> * Completed tool _thickness_ panel in _ToolPalette_.
> * Updated menu bar icons under _*File*_.


<br/>

##### [ May 28th 2019 ]
> * Switched labels in _Tool Palette_ to border labels.
> * Moved main tool palette into main window.
> * Added **Edit** menu with single _Copy_  menu item.
> * Added **Help** menu with _Readme_ and _About_ items.
> * Updated **File** menu by creating handlers for _New, Open, Save_ menu items.
> * Added basic _Canvas_ panel with colour indicator _DARK GREY_ to make it visible.


<br/>

##### [ May 29th 2019 ]
> * Playing with drawing on _Canvas_.
> * Added tool tips for _Readme_ and _Copy_ function.
> * Constrained the _JFileChooser_ to select single files.
> * Switched colour background of _tool panel_ to a pre-definable colour which can be tweaked in constructor.


<br/>

##### [ May 31st 2019 ]
> * Converting _ToolPalette_ to an extension of the JPanel; makes more sense as a self contained object.
> * Created custom *__JSKShape__* class inside _Canvas_ to describe shapes to be drawn.
> * Created ArrayList of _JSKShape_ objects to iterate through and draw in the _PaintComponent_.
> * Need to add _add_ method to _Canvas_ to add shapes to the scene.
> * Create constructor for _JSKShape_.


<br/>

##### [ June 1st 2019 ]
> * Added drawing loop to _paintComponent_.
> * Added a Bar & Tone test scene to constructor to test redraw.
> * Completed _New_ file operation to clear scene data and redraw the panel.
> * Added code to fill colours of shapes. Fill shapes and then stroke them to ensure border is above stroke.
> * Added code to set tools to be used on _Canvas_.


<br/>

##### [ June 2nd 2019 ]
> * Created enum space required required for _Tool Palette_ select tool toggling.
> * Moved button groups and buttons to the class level in _Tool Palette_.
> * Created method to update selected shapes colour and thickness.


<br/>

##### [ June 3rd 2019 ]
> * Created handlers in _Tool Palette_ and _Canvas_ to register each other.
> * Separated _ToolButton_ class.
> * Separated _ColourButton_ class.
> * Separated _ThicknessButton_ class.
> * Moved enums to _PainterButton_ class, makes more sense.


<br/>

##### [ June 4th 2019 ]
> * Working on event handlers for mouse in _Canvas_.
> * Moved _LoadIcon_ into the _PainterButton_ abstract class.


<br/>

##### [ June 7th 2019 ]
> * Created _mouse adapter_ class.
> * _Shape_ selection functional and updating _Tool Palette_, still need to figure out line selection and stroke dashing.
> * _Eraser_ tool is working.
> * _Fill_ tool working.
> * _Select_ tools _Thickness_ and _Colour_ switching functionality complete.
> * Created reference to _Canvas_ in _Tool Palette_ and reordered the constructors calls for the panels.
> * Removed redundant static methods and members from _Canvas_ to support _Select_ repaint operations.

<br/>

##### [ June 8th 2019 ]
> * Dash pattern for selected shapes is dynamic with actual border size.
> * Fixed _select_ tool functionality with selecting _line_ shapes.
> * Working on _select_ and _move_ functionality.


<br/>

##### [ June 9th 2019 ]
> * Created _move handler_.
> * Created _draw handler_.
> * Deselecting _shape_ on _ESC_ key press, help
    from comment #2: https://stackoverflow.com/questions/286727/unresponsive-keylistener-for-jframe.
    <br/>
>   _KeyboardFocusManager_ needs to override _dispatchKeyEvent_  in an anonymous instance of _KeyEventDispatcher_ object
    to access local variable _m_ActiveShapeID_ and then trigger _repaint_.
> * _Load/Save_ writing raw Java Objects to disk, _JFileChooser_ restricted to _.painter_ file extension.
    <br/>
>   Help from:
    <br/>
>   https://www.mkyong.com/java/how-to-write-an-object-to-file-in-java/
    <br/>
>   https://stackoverflow.com/questions/17293991/how-to-write-and-read-java-serialized-objects-into-a-file


<br/>

##### [ June 10th 2019 ]
> * Deselecting active shapes on tool change.
> * Fixed object serialization for _JSShape_.
> * _drawing handler_ potentially completed. Won't draw correctly without starting corner being in the top left.
> * _move handler_ completed (Stupid _AffineTransform_ and Java tutorial).
> * Removed redundant location data from _JSKShape_ and renamed it to _JSShape_.
> * Added *__Bar and Tone__* basic file _(BarAndTone.painter)_ for loading test.


<br/>

##### [ June 11th 2019 ]
> * _Drawing handler_ completed; fixed and working shape creation in any direction.
> * _Copy_ to system clipboard complete.
> * _Select_ now moves active shape to top and this allows for scene shape reordering.
> * Numerous bug fixes.
> * __Main preliminary feature set complete as per specs__.


<br/>

##### [ June 12th 2019 ]
> * `README.md` updated with system specs, features, and usage details.
> * `README.txt` added with all the information from `.md` file.


<br/>

##### [ June 13th 2019 ]
> * _Line_ shouldn't be a _Fill_-able object, feature updated.
> * Fixed bug in _Custom Colour_ selector where cancellation of the operation would set the colour to _Blue_.

<br/><br/>


##### [ October 6th 2020 ]
> * Began work prepping resource access to files to support fat jar creation.
> * Created and moved icons and images to `/src/main/resources/` directory to

<br/>

##### [ October 7th 2020 ]
> * Completed loading of all resources for _menu_ icons from `/src/main/resources/`.
> * Created static loader class in `PainterButton` to load all `ImageIcon`s from jar resource directory.
> * Updated `LoadIcon` to load files from `/src/main/resources/`.
> * Created `LoadAppIcon` to read and load all files from `/src/main/resources/`.
> * Switched over all image source linking to the `/src/main/resources/` folder.
> * Updated `gradle.build` to create fat jar files.
> * Added Legal and License information to the project.

<br/>

##### [ October 8th 2020 ]
> * Created `Painter-1.1.zip` in root directory which contains packaged application code.
> * `Squash`ed git commits and made the project public.

<br/>


---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>.</div>

---
